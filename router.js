import express from "express";
import { checkHealth } from "./controller/checkHealthController.js";
import { bio } from "./controller/biodataController.js";


const router = express.Router();

router.get('/check-health', checkHealth)
router.get('/biodata', bio)

export default router